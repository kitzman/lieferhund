(configuration
 (post-hook
  (hook-cons
   (lambda (_)
     (use-modules (srfi srfi-19))
     (format #t "started processing: ~a\n"
	     (date->string (current-date))))
   (hook-gen make-printer-hook)
   (hook-gen
    make-each-script-hook
    "cat '~~CONTENT~~' > \"/tmp/mailtest/~~OPT(newsgroup)~~.~~TITLE~~.~~DATE~~.txt\""
    '("Dear User,\n"
      "You are decent. Here are your news: \n\n"
      "Title: ~~TITLE~~\nDate: ~~DATE~~\n"
      "URL: ~~LINK~~\n\n"
      "~~DESCRIPTION~~\n"))))
 (store-size 250)
 (entry
  (name "guix ci")
  (url "http://ci.guix.gnu.org/events/rss/")
  (type rss)
  (opts (("newsgroup" . "org.gnu.guix.ci"))))
 (entry
  (name "gentoo news")
  (url "https://planet.gentoo.org/universe/rss20.xml")
  (type rss)
  (opts (("newsgroup" . "org.gentoo.planet"))))
(entry
  (name "arch news")
  (url "https://archlinux.org/feeds/news/")
  (type rss)
  (opts (("newsgroup" . "org.arch.news")))))
