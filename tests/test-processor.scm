;;
;; Copyright © 2021 kitzman <kitzman @ disroot . org>
;;

(use-modules (lieferhund processor)
	     (lieferhund config)
	     (lieferhund proto)
	     ((lieferhund db) :prefix db:)
	     (srfi srfi-1)
	     (srfi srfi-19)
	     (ice-9 match))
;;
;; Test data
;;

(define test-table-name "lwn")

(define test-table-url "https://lwn.net/headlines/newrss")

(define test-config-entry0
  (make-configuration-entry
   test-table-name
   test-table-url
   'dummy
   '(())))

(define test-config-entry1
  (make-configuration-entry
   test-table-name
   test-table-url
   'another-dummy
   '(())))

(define test-config
  (make-configuration
   10
   `(,test-config-entry0)
   #f))

(define test-config-multi
  (make-configuration
   10
   `(,test-config-entry0 ,test-config-entry1)
   #f))

(define test-items
  `((,test-config-entry0
     (,(make-feed-entry
	"a"
	"a desc"
	"scheme://lala_a"
	"20 June 2020 06:06:06 Z")
      ,(make-feed-entry
	"b"
	"b desc"
	"scheme://lala_b"
	"16 June 2020 06:06:06 Z")))))

(define test-items-multi
  `((,test-config-entry0
     (,(make-feed-entry
	"a"
	"a desc"
	"scheme://lala_a"
	"20 June 2020 06:06:06 Z")
      ,(make-feed-entry
	"b"
	"b desc"
	"scheme://lala_b"
	"16 June 2020 06:06:06 Z")))
    (,test-config-entry1
     (,(make-feed-entry
	"a"
	"a desc"
	"scheme://lala_a"
	"20 June 2020 06:06:06 Z")
      ,(make-feed-entry
	"b"
	"b desc"
	"scheme://lala_b"
	"16 June 2020 06:06:06 Z")))))

(define test-items-flattened
  `((,test-config-entry0
     ,(make-feed-entry
       "a"
       "a desc"
       "scheme://lala_a"
       "20 June 2020 06:06:06 Z"))
    (,test-config-entry0
     ,(make-feed-entry
       "b"
       "b desc"
       "scheme://lala_b"
       "16 June 2020 06:06:06 Z"))))

(define test-items-flattened-sorted
  `((,test-config-entry0
     ,(make-feed-entry
       "b"
       "b desc"
       "scheme://lala_b"
       "16 June 2020 06:06:06 Z"))
    (,test-config-entry0
     ,(make-feed-entry
       "a"
       "a desc"
       "scheme://lala_a"
       "20 June 2020 06:06:06 Z"))))

;;
;; Preprocessing functions
;;

(test-begin "preprocessing functions")

(test-equal "sorting"
  test-items-flattened-sorted
  ((@@ (lieferhund processor) preprocess-sort) test-items-flattened))

(test-equal "flatten"
  test-items-flattened
  ((@@ (lieferhund processor) preprocess-flatten) test-items))

(test-end "preprocessing functions")

;;
;; Processing function
;;

(test-begin "hook processing function")

(test-equal "no hook"
  '()
  (process test-config test-items #f))

(define result '())
(test-equal "process each hook"
  '("b" "a")
  (begin
    (process
     test-config
     test-items
     '(process-each
       (lambda (config-entry item)
	 (set! result (append result `(,(entry-title item)))))))
    result))


(define result '())
(test-equal "process each channel hook"
  test-items
  (begin
    (process
     test-config
     test-items
     '(process-each-channel
       (lambda (config-entry items)
	 (set!
	  result
	  (cons
	   `(,config-entry ,items)
	   result)))))
    result))

(test-equal "hook generation"
  '(("xa" "xb"))
  (process
   test-config
   test-items
   '(hook-gen
     (lambda (p)
       (lambda (pair-entries)
	 (map
	  (lambda (pair-entry)
	    (map
	     (lambda (item)
	       (string-append p (entry-title item)))
	     (cadr pair-entry)))
	  pair-entries)))
     "x")))


(define result '())
(test-equal "hook cons"
  '(#t #t)
  (begin
    (process
     test-config
     test-items
     '(hook-cons
       (lambda (_)
	 (set!
	  result
	  (cons #t result)))
       (lambda (_)
	 (set!
	  result
	  (cons #t result)))))
    result))

(define result '())
(test-equal "hook match type"
  '((#t) (#t))
  (begin
    (process
     test-config
     test-items
     `(hook-match-type
       (dummy
	(lambda (_)
	  (set!
	   result
	   (cons '(#t) result))))
       (dummy-another
	(lambda (_)
	  (set!
	   result
	   (cons '(#t) result))))))
    (format #t "~a\n" result)
    result))

(test-equal "hook generic"
  #t
  (process
   test-config
   test-items
   '(lambda (_) #t)))


(test-end "hook processing function")
