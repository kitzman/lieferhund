;;
;; Copyright © 2021 kitzman <kitzman @ disroot . org>
;;

(use-modules (lieferhund proto)
	     (lieferhund config)
	     (lieferhund db))

;;
;; Test data
;;

(fluid-set!
 lieferhund-proto-map
 `((dummy0 . (,(lambda (config-entry) '(a))
	      ,(lambda (response-items) '(a))))
   (dummy1 . (,(lambda (config-entry) #f)
	      ,(lambda (response-items) '(a))))
   (dummy2 . (,(lambda (config-entry) '(a))
	      ,(lambda (response-items) '())))))

;; Db data
(define test-table-name0 "lwn")

(define test-table-url0 "https://lwn.net/headlines/newrss")

(define test-config-entries
  `(,(make-configuration-entry
      test-table-name0
      test-table-url0
      'dummy0
      '(()))
    ,(make-configuration-entry
      test-table-name0
      test-table-url0
      'dummy1
      '(()))
    ,(make-configuration-entry
      test-table-name0
      test-table-url0
      'dummy2
      '(()))
    ,(make-configuration-entry
      test-table-name0
      test-table-url0
      'dummy3
      '(()))))

(define test-config
  (make-configuration
   10
   test-config-entries
   #f))

(define test-entries0
  `(,(make-feed-entry
      "Security updates for Saturday"
      "a"
      "https://lwn.net/Articles/862487/rss"
      "10 July 2021 23:58:13 Z")
    ,(make-feed-entry
      "Announcing Arti, a pure-Rust Tor implementation (Tor blog)"
      "b"
      "https://lwn.net/Articles/862329/rss"
      "09 July 2021 18:05:12 Z")))

(define test-table
  (make-channel-table
   test-table-name0
   test-table-url0
   (false-if-exception
    (string->date
     "11 July 2021 20:45:25 +0300"
     db-date-format))
   test-entries0))

(define test-db
  (make-channel-database
   (database-tables-list->database-tables
    `(,test-table))))

;;
;; Proto map functions
;;

(test-begin "testing the proto map functions")

(test-assert "map retrieval fn, correct proto behaviour"
  ((@@ (lieferhund proto) proto-map-retrieval-fn) 'dummy0))

(test-assert "map retrieval fn, wrong proto behaviour"
  (not ((@@ (lieferhund proto) proto-map-retrieval-fn) 'dummy9)))

(test-assert "map parsing fn, correct proto behaviour"
  ((@@ (lieferhund proto) proto-map-parse-fn) 'dummy0))

(test-assert "map parsing fn, wrong proto behaviour"
  (not ((@@ (lieferhund proto) proto-map-parse-fn) 'dummy9)))

;;
;; Retrieval test
;;

(test-begin "testing the retrieval of entries")

(test-assert "failsafety in case feed type is not found"
  (not
   (retrieve-feed-entries
    (cadddr test-config-entries)
    test-config
    test-db)))

(test-equal "failed retrieval"
  `(,(cadr test-config-entries) ())
  (retrieve-feed-entries
   (cadr test-config-entries)
   test-config
   test-db))

(test-equal "failed parsing"
  `(,(caddr test-config-entries) ())  
   (retrieve-feed-entries
    (caddr test-config-entries)
    test-config
    test-db))

(test-equal "working feed retrieval"
  `(,(car test-config-entries) (a))
  (retrieve-feed-entries
   (car test-config-entries)
   test-config
   test-db))

(test-end "testing the retrieval of entries")
