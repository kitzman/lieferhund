;;
;; Copyright © 2021 kitzman <kitzman @ disroot . org>
;;

(use-modules (lieferhund db)
	     (lieferhund proto)
	     (lieferhund config)
	     (srfi srfi-1)
	     (srfi srfi-64)
	     (srfi srfi-19)
	     (srfi srfi-69)
	     (ice-9 pretty-print))

;;
;; Utilities
;;

(define (db->sexp-db db)
  (make-channel-database
   (hash-table->alist (database-tables db))))

;;
;; Test data
;;

;; Date data
(define test-db-date-str "10 June 2020 16:16:09 Z")
(define test-db-date (make-date 0 9 16 16 10 6 2020 0))

;; Db data
(define test-table-name0 "lwn")
(define test-table-name1 "julia evans")
(define test-table-name2 "lala")

(define test-table-url0 "https://lwn.net/headlines/newrss")
(define test-table-url1 "https://jvns.ca/atom.xml")

(define test-config-entries
  `(,(make-configuration-entry
      test-table-name0
      test-table-url0
      'rss
      '(()))
    ,(make-configuration-entry
      test-table-name1
      test-table-url1
      'rss
      '(()))
    ,(make-configuration-entry
      test-table-name2
      test-table-url1
      'rss
      '(()))))

(define test-config
  (make-configuration
   10
   test-config-entries
   #f))

(define test-entries0
  `(,(make-feed-entry
      "Security updates for Saturday"
      "a"
      "https://lwn.net/Articles/862487/rss"
      "10 July 2021 23:58:13 Z")
    ,(make-feed-entry
      "Announcing Arti, a pure-Rust Tor implementation (Tor blog)"
      "b"
      "https://lwn.net/Articles/862329/rss"
      "09 July 2021 18:05:12 Z")))

(define test-entries1
  `(,(make-feed-entry
      "Write good examples by starting with real code"
      ""
      "https://jvns.ca/blog/2021/07/08/writing-great-examples/"
      "08 July 2021 11:00:46 Z")
    ,(make-feed-entry
       "Reasons why bugs might feel \"impossible\""
       ""
       "https://jvns.ca/blog/2021/06/08/reasons-why-bugs-might-feel-impossible/"
       "08 June 2021 09:28:08 Z")))

(define test-extra-entries
  `(,(make-feed-entry
      "Random title"
      "Random description"
      "https://lwn.net/Articles/1234567890/rss"
      "16 July 2021 18:05:12 Z")))

(define test-table0
  (make-channel-table
   test-table-name0
   test-table-url0
   (false-if-exception
    (string->date
     "11 July 2021 20:45:25 +0300"
     db-date-format))
   test-entries0))

(define test-table0-trunc
  (make-channel-table
   test-table-name0
   test-table-url0
   (false-if-exception
    (string->date
     "11 July 2021 20:45:25 +0300"
     db-date-format))
   `(,(car test-entries0))))

(define test-table1
  (make-channel-table
   test-table-name1
   test-table-url1
   (false-if-exception
    (string->date
     "11 July 2021 20:45:27 +0300"
     db-date-format))
   test-entries1))

(define test-db
  (make-channel-database
   (database-tables-list->database-tables
    `(,test-table0 ,test-table1))))

;;
;; Date tests
;;

(test-begin "testing date format")

(test-equal "to-string conversion"
  test-db-date
  (string->date test-db-date-str db-date-format))

(test-equal "from-string conversion"
  test-db-date-str
  (date->string test-db-date db-date-format))

(test-end "testing date format")

;;
;; Database parsing
;;

(define (db-sample no)
  (format #f "tests/data/sample-db~a.scm" no))

(test-begin "database parsing")

(test-equal "db reading"
  (db->sexp-db test-db)
  (db->sexp-db (read-database (db-sample 0))))

(test-equal "db writing, and consistency check"
  (db->sexp-db test-db)
  (let* ((directory (or (getenv "TMPDIR") "/tmp"))
	 (template (string-append directory "/lieferhund.XXXXXX"))
	 (out (mkstemp! template))
	 (_ (close out))
	 (_ (save-database template test-db))
	 (_ (flush-all-ports))
	 (rdb (read-database template))
	 (_ (delete-file template)))
    (db->sexp-db rdb)))

(test-end "database parsing")

;;
;; Database operations
;;

(test-begin "database operations")

(test-assert "db exists"
  (and (database-has-table test-db test-table-name0)
       (database-has-table test-db test-table-name1)))

(test-group
 "db get"
 (test-equal "table 0"
   test-table0
   (database-get test-db test-table-name0))
 (test-equal "table 1"
   test-table1
   (database-get test-db test-table-name1)))

(test-equal
    "db insert (existing table, no new config entries)"
  test-extra-entries
  (database-insert-entries!
   test-db
   test-config
   (car test-config-entries)
   test-extra-entries))

(test-equal
    "db insert (new table, new config tables))"
  test-extra-entries
  (database-insert-entries!
   test-db
   test-config
   (caddr test-config-entries)
   test-extra-entries))

(test-equal
    "db truncation"
  test-table0-trunc
  ((@@ (lieferhund db) table-cleanup) test-table0 1))

(test-begin "database operations")
