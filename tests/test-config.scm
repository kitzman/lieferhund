;;
;; Copyright © 2021 kitzman <kitzman @ disroot . org>
;;

(use-modules (lieferhund config)
	     (lieferhund util)
	     (srfi srfi-64)
	     (ice-9 pretty-print))

;;
;; Sample configurations
;;

(define sample-store-size 250)

(define sample-entries
  `(,(make-configuration-entry
      "guix ci"
      "http://ci.guix.gnu.org/events/rss/"
      'rss
      '(("newsgroup" . "org.gnu.guix.ci")))
    ,(make-configuration-entry
      "arch news"
      "https://archlinux.org/feeds/news/"
      'rss
      '(("newsgroup" . "org.arch.news")))))

(define sample-post-hook
  '(hook-cons
    (hook-gen make-printer-hook)))

(define sample-configuration
  (make-configuration
   sample-store-size
   sample-entries
   sample-post-hook))
  
;;
;; Configuration expressions
;;

(define (config-sample-filename no)
  (format #f "tests/data/sample-config~a.scm" no))

;;
;; Testing reading/writing
;;

(test-begin "reading the configuration")
 
(test-equal "correct configuration"
  sample-configuration
  (read-config-file (config-sample-filename 0)))
 
(test-error
 "missing post-hook"
 (read-config-file (config-sample-filename 1)))
 
(test-error
 "missing store size"
 (read-config-file (config-sample-filename 2)))
 
(test-error
 "missing name"
 (read-config-file (config-sample-filename 3)))
 
(test-error
 "missing url"
 (read-config-file (config-sample-filename 4)))

(test-error
 "missing type"
 (read-config-file (config-sample-filename 5)))

(test-error
 "missing opts"
 (read-config-file (config-sample-filename 6)))

(test-end "reading the configuration")

(test-begin "writing the configuration")

(test-equal "saving to a file, checking consistency"
  sample-configuration
  (let* ((directory (or (getenv "TMPDIR") "/tmp"))
	 (template  (string-append directory "/lieferhund.XXXXXX"))
         (out (mkstemp! template))
	 (_ (close out))
	 (_ (save-config-file template sample-configuration))
	 (_ (flush-all-ports))
	 (rconfig (read-config-file template))
	 (_ (delete-file template)))
    rconfig))

(test-end "writing the configuration")
