(configuration
 (store-size 250)
 (entry
  (name "guix ci")
  (url "http://ci.guix.gnu.org/events/rss/")
  (type rss)
  (opts (("newsgroup" . "org.gnu.guix.ci"))))
 (entry
  (name "arch news")
  (url "https://archlinux.org/feeds/news/")
  (type rss)
  (opts (("newsgroup" . "org.arch.news")))))
