# Synopsis

Lieferhund is a (the most?) hackable news-reading utility.

To use it, first make sure you have a configuration file in place - the default
location is in `$HOME/.config/lieferhund/config.scm`. Then, the first step
would be to pulling the news from the sources.

```
lieferhund -x pull
lieferhund -c $CONFIG_FILE -b $DB_FILE -x pull
```

Next, one can invoke the `read` command (which is quite basic at this moment),
to go through the news:

```
lieferhund -x read
```

# Supported protocols

Currently, only RSS and Atom feeds are supported, but mailing lists are on their way.

# Configuration hacking

An example configuration can be found in `examples/`.

One just needs to add entries.

The hackability comes into attention, when writing the `'post-hook` field in the
configuration. A post hook is defined as:

  - `#f` - no action, just add to the database
  - a lambda, which takes as argument, a list of the form `'((config-entry news-items) ...)`
  - an exp of the form `('process-each hook-fn)`, which applies the lambda with the signature
	`(lambda (config-entry item) ...)`, to each news item, in a sorted manner
  - an exp of the form `('process-each-channel hook-fn)`, which applies the lambda with
	the signature `(lambda (config-entry news-items) ...)`, to each channel and its
	entries
  - `('hook-gen make-hook args ...)` applies the hook generator, with the arguments specified
	(i.e: `(hook-gen make-send-mail-to "xxx@yyy.zzz")`)
  - `('hook-cons hooks ...)` which means that multiple hooks can be combined

There are already 2 hooks already implemented:

  - `make-printer-hook` - takes no argument, prints each item
  - `make-each-script-hook` - takes two arguments - a string describing the
	command, and a list with strings which describe the content of the message,
	which is written to a temporary file and can be used in the command; to make
	the process useful, the following string interpolations are defined:
	`~~CONTENT~~` - the content file, `~~NAME~~` - config entry name, `~~TITLE~~` -
	item's title, `~~DATE~~` - item's date, `~~DESCRIPTION~~` - the item's description,
	and `~~OPT(option_name)~~` for accessing a user defined option

To be able to write and use custom hooks, each configuration entry has a field `opts`,
which is an association list.

Additionally, in `lieferhund/interpolator.scm`, one can find functions that can 
interpolate different type of information, including the entry options.

# Treating your delivery doggo well

```
lieferhund -x give-treat
```

# The Daemon

If you'd prefer running this as a daemon, there is `lieferwelpe.scm`, which only periodically
pulls and processes the news. This can be used either on a server (in case you wouldn't prefer
crontabs), or locally (i.e: send notifications via dbus). The news ofc can be read with the
`lieferhund.scm` tool.

# TODOs

For active TODOs, please check the issue tab.

# Building and installing

Please the the `HACKING`, `INSTALL`.

Eventually, this will be packaged for guix when v1.0 comes out and hopefully, the channel will be merged into the
official Guix upstream.

