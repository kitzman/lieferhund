(hall-description
  (name "lieferhund")
  (prefix "")
  (version "0.6")
  (author "kitzman")
  (copyright (2021))
  (synopsis "")
  (description "")
  (home-page
    "https://git.disroot.org/kitzman/lieferhund")
  (license gpl3+)
  (dependencies `())
  (files (libraries
           ((scheme-file "lieferhund")
            (directory "lieferhund" ())))
         (tests ((directory "tests" ())))
         (programs ((directory "scripts" ())))
         (documentation
           ((org-file "README")
            (symlink "README" "README.org")
            (text-file "HACKING")
            (text-file "COPYING")
            (directory "doc" ((texi-file "lieferhund")))))
         (infrastructure
           ((scheme-file "guix")
            (text-file ".gitignore")
            (scheme-file "hall")))))
