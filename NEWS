-*- mode: org; coding: utf-8; -*-

#+TITLE: Lieferhund NEWS – history of user-visible changes
#+STARTUP: content hidestars

Copyright © (2021) kitzman <kitzman @ disroot . org>

  Copying and distribution of this file, with or without modification,
  are permitted in any medium without royalty provided the copyright
  notice and this notice are preserved.

Please send Lieferhund bug reports to kitzman.

* Publication at 0.6
  - there was a bug - when the config-entry did not exist
    in the db, the script would fail, because it tried to
    retrieve from the db that specific entry, and it would
    have failed
  - Atom support
  - a directive to match, inside the post hook, the channel type
  - should the data retrieval fail, now it just fails with
    an error message and the processing continues
  - now the database is saved after processing - if a fatal error
    occurs during processing, this allows for the entries to be
    re-processed

* Publication at 0.5
  - compatibility with guile 2.2 - (ice-9 string-fun) libraries
    obviously differ between versions - now one the functions
    is implemented in utils.scm, so then this tool is compatible
    with both guile 3.0 and guile 2.2
  - RSS parsing was before incomplete - now it should work according
    to the specification
  - the ability to have multiple sources (RSS, etc)

* Publication at 0.2
  - PEG parser is implemented - this might help in the future
    in case recursive interpolations are done
  - user defined options - an alist in the configuration to allow
    the user to insert their own options, and retrieve them when
    the post-hook runs
