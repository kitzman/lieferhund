;;
;; Copyright © 2021 kitzman <kitzman @ disroot . org>
;;

(use-modules
  (guix packages)
  ((guix licenses) #:prefix license:)
  (guix download)
  (guix build-system gnu)
  (gnu packages)
  (gnu packages autotools)
  (gnu packages guile)
  (gnu packages guile-xyz)
  (gnu packages pkg-config)
  (gnu packages texinfo))

(package
  (name "lieferhund")
  (version "0.6")
  (source "./lieferhund-0.6.tar.gz")
  (build-system gnu-build-system)
  (arguments `())
  (native-inputs
    `(("autoconf" ,autoconf)
      ("automake" ,automake)
      ("pkg-config" ,pkg-config)
      ("texinfo" ,texinfo)))
  (inputs `(("guile" ,guile-3.0)))
  (propagated-inputs `())
  (synopsis "A hackable news processor and reader.")
  (description "(the most?) hackable news processor and reader.")
  (home-page
    "https://git.disroot.org/kitzman/lieferhund")
  (license license:gpl3+))

