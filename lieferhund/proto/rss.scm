;;
;; Copyright © 2021 kitzman <kitzman @ disroot . org>
;;

(define-module (lieferhund proto rss)
  #:use-module (lieferhund proto)
  #:use-module ((lieferhund db) :prefix db:)
  #:use-module (web client)
  #:use-module (web response)
  #:use-module (sxml simple)
  ;; #:use-module (sxml match)
  #:use-module ((sxml xpath) :prefix xpath:)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-19)
  #:use-module (ice-9 textual-ports)
  #:use-module (ice-9 match)
  ;; #:use-module (ice-9 suspendable-ports)

  #:export (request-rss-feed
	    parse-rss-feed-response))

;; (install-suspendable-ports!)

;;
;; Date format
;;

(define rss2.0-date-format "~a, ~d ~b ~Y ~H:~M:~S ~z")
(define rss1.0-date-format "~Y-~m-~dT~H:~M:~S~z")

;;
;; RSS feed retrieval function
;;

(define (request-rss-feed uri)
  (let* ((raw-response (http-get uri #:decode-body? #f #:streaming? #t))
	 (response-code (response-code raw-response))
	 (response-content-type
	  (symbol->string
	   (car (response-content-type raw-response)))))
    (if (and
	 (= response-code 200)
	 (or (string-suffix? "/xml" response-content-type)
	     (string-suffix? "/rss+xml" response-content-type)))
        (xml->sxml
	 (get-string-all (response-body-port raw-response #:decode? #f)))
	#f)))

;;
;; RSS utilities
;;

(define rss-tree-id
  '(rss
    #{http://www.w3.org/1999/02/22-rdf-syntax-ns#:RDF}#))

(define (rss-tree? tree)
  (and (list? tree)
       (fold (lambda (t a) (or (eq? (car tree) t) a))
	#f rss-tree-id)))

(define rss-channel-id
  '(channel
    http://purl.org/rss/1.0/:channel))

(define (rss-channel? tree)
  (and (list? tree)
       (fold (lambda (t a) (or (eq? (car tree) t) a))
	#f rss-channel-id)))

(define rss-item-id
  '(item
    http://purl.org/rss/1.0/:item))

(define (rss-item? tree)
  (and (list? tree)
       (fold (lambda (t a) (or (eq? (car tree) t) a))
	     #f rss-item-id)))
;;
;; RSS feed specific methods
;;

(define (rss-feed-rss tree)
  (let ((tree-rss (filter rss-tree? tree)))
    (match tree-rss
      ('() #f)
      (_ (car tree-rss)))))

(define (rss-feed-channel tree)
  (let ((tree-channel (filter rss-channel? tree)))
    (match tree-channel
      ('() #f)
      (_ (car tree-channel)))))

(define (get-title tree)
  (let ((tree-channel-title
	 (filter (lambda (t)
		   (and (list? t)
			(eq? 'title (car t))))
		 tree)))
    (match tree-channel-title
      ('() #f)
      (_ (cadar tree-channel-title)))))

(define (get-items tree)
  (let ((items (filter rss-item? tree)))
    (match items
      ('() #f)
      (_ items))))

(define (get-entry-value entry-value field)
  (define title-syms '(title
		       http://purl.org/rss/1.0/:title))
  (define link-syms '(link
		      http://purl.org/rss/1.0/:link))
  (define description-syms '(description
			     http://purl.org/rss/1.0/:description))
  (define pubDate-syms '(pubDate
			 http://purl.org/dc/elements/1.1/:date))

  (define (sxml-match-one-of tree mlist)
    (or-map
     (lambda (me)
       (let ((te ((xpath:sxpath `(,me)) tree)))
	 (match te
	   (() #f)
	   (_ (cadar te)))))
     mlist))

  ;; (format #t "^^^ ~a\n" entry-value)
  
  (match entry-value
    (() #f)
    (_
     (match field
       ('title (sxml-match-one-of entry-value title-syms))
       ('link (sxml-match-one-of entry-value link-syms))
       ('description (sxml-match-one-of entry-value description-syms))
       ('pubDate (sxml-match-one-of entry-value pubDate-syms))
       (_ #f)))))
       
(define (parse-rss-feed-entry tree)
  (let* ((title (get-entry-value tree 'title))
	 (link (get-entry-value tree 'link))
	 (description-pre (get-entry-value tree 'description))
	 (description (if (equal? description-pre #f)
			  "" description-pre))
	 (pub-date-pre (get-entry-value tree 'pubDate))
	 (pub-date
	  (or
	   (false-if-exception
	    (date->string
	     (string->date
	      pub-date-pre
	      rss2.0-date-format)
	     db:db-date-format))
	   (false-if-exception
	    (date->string
	     (string->date
	      (let* ((date-split (string-split pub-date-pre #\:))
		     (tz-pos (- (length date-split) 1))
		     (date-tz (string-join (list-tail
					    date-split
					    tz-pos) ""))
		     (date-rest (string-join
				 (list-head date-split tz-pos)
				 ":"))
		     (date-final (string-append date-rest date-tz)))
		date-final)
	      rss1.0-date-format)
	     db:db-date-format)))))
    `(,title ,description ,link ,pub-date)))

;;
;; Final processor that returns rss feed entries
;;

(define (parse-rss-feed-response response)
  (let* ((rss (rss-feed-rss response))
	 (channel (and=> rss rss-feed-channel))
	 (channel-title (and=> channel get-title))
	 (channel-items (or (and=> channel get-items)
			    (and=> rss get-items))))
    (map (lambda (i) (parse-rss-feed-entry i))
	 channel-items)))
