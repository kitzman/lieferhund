;;
;; Copyright © 2021 kitzman <kitzman @ disroot . org>
;;

(define-module (lieferhund proto atom)
  #:use-module (lieferhund proto)
  #:use-module (lieferhund util)
  #:use-module ((lieferhund db) :prefix db:)
  #:use-module (web client)
  #:use-module (web response)
  #:use-module (sxml simple)
  ;; #:use-module (sxml match)
  #:use-module ((sxml xpath) :prefix xpath:)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-19)
  #:use-module (ice-9 textual-ports)
  #:use-module (ice-9 match)
  ;; #:use-module (ice-9 suspendable-ports)

  #:export (request-atom-feed
	    parse-atom-feed-response))

;; (install-suspendable-ports!)

;;
;; Atom date format
;;

(define atom-date-format "~Y-~m-~dT~H:~M:~S~z")

;;
;; Atom feed retrieval function
;;

(define (request-atom-feed uri)
  (let* ((raw-response (http-get uri #:decode-body? #f #:streaming? #t))
	 (response-code (response-code raw-response))
	 (response-content-type
	  (symbol->string
	   (car (response-content-type raw-response)))))
    (if (and
	 (= response-code 200)
	 (or (string-suffix? "/xml" response-content-type)
	     (string-suffix? "/rss+xml" response-content-type)
	     (string-suffix? "/atom+xml" response-content-type)))
        (xml->sxml
	 (get-string-all (response-body-port raw-response #:decode? #f)))
	#f)))

(define (sxml-get-text tree)
  (filter
   (lambda (e) ((xpath:node-typeof? '*text*) e))
   tree))

(define (sxml-get-href tree)
  (or-map
   (lambda (e) (match e
		 ((@ (href link) _ ...) link)
		 (_ #f)))
   tree))

;;
;; Atom specific methods
;;

(define atom-tree-id
  '(feed
    http://www.w3.org/2005/Atom:feed))

(define (atom-tree? tree)
  (and (list? tree)
       (fold (lambda (t a)
	       (or (eq? (car tree) t) a))
	#f atom-tree-id)))

(define atom-entry-id
  '(entry
    http://www.w3.org/2005/Atom:entry))

(define (atom-entry? tree)
  (and (list? tree)
       (fold (lambda (t a) (or (eq? (car tree) t) a))
	     #f atom-entry-id)))

(define atom-title-id
  '(title
    http://www.w3.org/2005/Atom:title))

(define (atom-title? tree)
  (and (list? tree)
       (fold (lambda (t a) (or (eq? (car tree) t) a))
	     #f atom-title-id)))

;;
;; Atom feed specific methods
;;

(define (atom-feed-atom tree)
  (let ((tree-atom (filter atom-tree? tree)))
    (match tree-atom
      ('() #f)
      (_ (car tree-atom)))))

(define (get-title tree)
  (let ((tree-channel-title (filter atom-title? tree)))
    (match tree-channel-title
      ('() #f)
      (_ (cadar tree-channel-title)))))

(define (get-entries tree)
  (let ((items (filter atom-entry? tree)))
    (match items
      ('() #f)
      (_ items))))

;;
;; Functions that parses entries
;;

(define (get-entry-value entry-value field)
  (define title-syms '(title
		       http://www.w3.org/2005/Atom:title))
  (define link-syms '(link
		      http://www.w3.org/2005/Atom:link))
  (define summary-syms '(summary
			 http://www.w3.org/2005/Atom:summary))
  (define updated-syms '(updated
			 http://www.w3.org/2005/Atom:updated))

  (define (sxml-match-one-of tree mlist)
    (or-map
     (lambda (me)
       (let ((te ((xpath:sxpath `(,me)) tree)))
	 (match te
	   (() #f)
	   (_ (or (car=> (sxml-get-text (car te)))
		  (sxml-get-href (car te)))))))
     mlist))
  
  (match entry-value
    (() #f)
    (_
     (match field
       ('title (sxml-match-one-of entry-value title-syms))
       ('link (sxml-match-one-of entry-value link-syms))
       ('summary (sxml-match-one-of entry-value summary-syms))
       ('updated (sxml-match-one-of entry-value updated-syms))
       (_ #f)))))

(define (parse-atom-feed-entry tree)
  (let* ((title (get-entry-value tree 'title))
	 (link (get-entry-value tree 'link))
	 (summary-pre (get-entry-value tree 'summary))
	 (summary (or summary-pre ""))
	 (updated-date-pre (get-entry-value tree 'updated))
	 (updated-date
	  (or
	   (false-if-exception
	    (date->string
	     (string->date
	      (let* ((date-split (string-split updated-date-pre #\:))
		     (tz-pos (- (length date-split) 1))
		     (date-tz (string-join (list-tail
					    date-split
					    tz-pos) ""))
		     (date-rest (string-join
				 (list-head date-split tz-pos)
				 ":"))
		     (date-final (string-append date-rest date-tz)))
		date-final)
	      atom-date-format)
	     db:db-date-format))
	   (false-if-exception
	    (date->string
	     (string->date
	      updated-date-pre
	      atom-date-format)
	     db:db-date-format)))))
    ;; (format #t "%%% ~a\n" link)
    `(,title ,summary ,link ,updated-date)))

;;
;; Final processor that returns atom feed entries
;;

(define (parse-atom-feed-response response)
  (let* ((atom (atom-feed-atom response))
	 (feed-title (and=> atom get-title))
	 (feed-items (and=> atom get-entries)))
    (map (lambda (i) (parse-atom-feed-entry i))
	 feed-items)))




