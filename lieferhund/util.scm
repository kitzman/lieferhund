;;
;; Copyright © 2021 kitzman <kitzman @ disroot . org>
;;


(define-module (lieferhund util)
  #:export (call-with-temporary-output-file
	    string-replace-substring
	    
	    and<=
	    and<=>

	    car=>))

;;
;; Copyright © FSF, Guix (guix/utils.scm)
;;

(define (call-with-temporary-output-file proc)
  (let* ((directory (or (getenv "TMPDIR") "/tmp"))
         (template  (string-append directory "/lieferhund.XXXXXX"))
         (out       (mkstemp! template)))
    (dynamic-wind
      (lambda ()
        #t)
      (lambda ()
        (proc template out))
      (lambda ()
        (false-if-exception (close out))
        (false-if-exception (delete-file template))))))

;;
;; Copyright © FSF, A. Wingo
;; string-replace-substring: guile 2.2's ice-9 string-fun is missing this
;;
(define (string-replace-substring str substring replacement)
  (let ((sublen (string-length substring)))
    (with-output-to-string
      (lambda ()
        (let lp ((start 0))
          (cond
           ((string-contains str substring start)
            => (lambda (end)
                 (display (substring/shared str start end))
                 (display replacement)
                 (lp (+ end sublen))))
           (else
            (display (substring/shared str start)))))))))

;;
;; and<= + and<=>
;;

(define (and<= proc val)
  (if (eq? proc #f)
      #f
      (proc val)))

(define (and<=> proc val)
  (if (or (eq? proc #f) (eq? val #f))
      #f
      (proc val)))

;;
;; car or #f
;;

(define (car=> lst)
  (if (pair? lst) (car lst) #f))
      
