;;
;; Copyright © 2021 kitzman <kitzman @ disroot . org>
;;

(define-module (lieferhund interpolator)
  #:use-module (lieferhund config)
  #:use-module (lieferhund proto)
  #:use-module (lieferhund util)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 match)
  #:use-module (ice-9 peg)
  ;; #:use-module (ice-9 string-fun)

  #:export (string-escaper

	    hook-string-interpolator
	    hook-string-interpolator-l))

;;
;; String interpolation functions
;;

(define (string-escaper s)
  (let* ((replacement-list
	  '(("\"" "\\\"")
	    ("$" "\\$"))))
    (fold
     (match-lambda*
       (((int-orig int-repl) sx)
	(string-replace-substring sx int-orig int-repl)))
     s
     replacement-list)))

(define (hook-string-interpolator s config-entry news-item content-file)
  (let* ((name (config-entry-name config-entry))
	 (opts (config-entry-opts config-entry))
	 (item-title (string-escaper (entry-title news-item)))
	 (item-date (entry-date news-item))
	 (item-link (entry-link news-item))
	 (item-desc (entry-description news-item)))

    (define-peg-pattern opt-arg-name-term all
      (+ (or (range #\a #\z)
	     (range #\A #\Z)
	     (range #\0 #\9)
	     "-" "_")))
    (define-peg-pattern opt-arg-term all
      (and "OPT(" opt-arg-name-term ")"))
    (define-peg-pattern const-term all
      (or "NAME" "TITLE" "DATE" "LINK" "DESCRIPTION" "CONTENT"))
    (define-peg-pattern field-term body
      (and "~~" (or const-term opt-arg-term) "~~"))

    (define ts s)

    (do ((mpat (search-for-pattern field-term ts)
	       (search-for-pattern field-term ts)))
	((equal? mpat #f))
      (match (cadr (peg:tree mpat))
	(('const-term "NAME")
	 (set! ts (string-replace ts name (peg:start mpat) (peg:end mpat))))
	(('const-term "TITLE")
	 (set! ts (string-replace ts item-title (peg:start mpat) (peg:end mpat))))
	(('const-term "DATE")
	 (set! ts (string-replace ts item-date (peg:start mpat) (peg:end mpat))))
	(('const-term "LINK")
	 (set! ts (string-replace ts item-link (peg:start mpat) (peg:end mpat))))
	(('const-term "DESCRIPTION")
	 (set! ts (string-replace ts item-desc (peg:start mpat) (peg:end mpat))))
	(('const-term "CONTENT")
	 (set! ts (string-replace ts content-file (peg:start mpat) (peg:end mpat))))
	(('opt-arg-term "OPT(" ('opt-arg-name-term opt-name) ")")
	 (let* ((opt-value (assoc-ref opts opt-name))
		(opt-string-value
		 (if (equal? opt-value #f)
		     "" (format #f "~a" opt-value))))
	   (set! ts (string-replace ts opt-string-value (peg:start mpat) (peg:end mpat)))))))
    ts))

(define (hook-string-interpolator-l exp config-entry news-item)
  (hook-string-interpolator (string-join exp "") config-entry news-item ""))
