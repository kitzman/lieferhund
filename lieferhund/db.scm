;;
;; Copyright © 2021 kitzman <kitzman @ disroot . org>
;;

(define-module (lieferhund db)
  #:use-module (lieferhund config)
  #:use-module (lieferhund proto)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-19)
  #:use-module (srfi srfi-69)
  #:use-module (ice-9 match)
  #:use-module (ice-9 pretty-print)
  #:use-module (ice-9 textual-ports)

  #:export (db-date-format

	    <channel-table>
	    make-channel-table
	    channel-table-name
	    channel-table-channel-url
	    channel-table-last-updated
	    channel-table-entries

	    <channel-database>
	    make-channel-database
	    database-tables

	    database-tables-list->database-tables
	    database-tables->database-tables-list

	    read-database
	    save-database

	    database-get
	    database-has-table
	    database-insert-entries!))

;;
;; Constants
;;

(define db-date-format "~d ~B ~Y ~H:~M:~S ~z")
(define db-lock-filename
  (if (getenv "TMP")
      (string-append (getenv "TMP") "/lieferhund-db.lock")
      "/tmp/lieferhund-db.lock"))
(define db-lock-interval 100000)

;;
;; Database locking functions
;;

(define (db-lock-is-locked?)
  (sync)
  (file-exists? db-lock-filename))

(define (database-lock-wait)
  (sync)
  (while (db-lock-is-locked?)
    (usleep db-lock-interval)
    (sync)))

(define (db-lock-lock)
  (database-lock-wait)
  (close (open-file db-lock-filename "w"))
  (sync))

(define (db-lock-unlock)
  (delete-file db-lock-filename)
  (sync))

(define-syntax call-with-db-lock
  (syntax-rules ()
    ((call-with-db-lock body ...)
     (let* ((_ (db-lock-lock))
	    (body-result (begin body ...))
	    (_ (db-lock-unlock)))
       body-result))))

;;
;; Database records
;;

(define-record-type <channel-table>
  (make-channel-table table-name channel-url last-updated entries)
  channel-table?
  (table-name channel-table-name)
  (channel-url channel-table-channel-url)
  (last-updated channel-table-last-updated)
  (entries channel-table-entries))

(define-record-type <channel-database>
  (make-channel-database tables)
  channel-database?
  (tables database-tables))

(define (parse-channel-entry exp)
  (match exp
    (('entry ('title title)
	     ('description description)
	     ('link link)
	     ('date date))
     (make-feed-entry title description link date))))

(define (parse-database-table exp)
  (match exp
    (('table
      ('channel-name channel-name)
      ('channel-url channel-url)
      ('last-updated last-updated)
      ('entries entries))
     (make-channel-table
      channel-name channel-url (string->date last-updated db-date-format)
      (map parse-channel-entry entries)))))

(define (database-tables-list->database-tables tables-list)
  (let ((tables (make-hash-table)))
    (for-each
     (lambda (table)
       (hash-table-set! tables (channel-table-name table) table))
     tables-list)
    tables))

(define (database-tables->database-tables-list tables)
  (hash-table-fold
   tables
   (lambda (k v p)
     (cons v p))
   '()))

(define (parse-database exp)
  (make-channel-database
   (match exp
     (('database tables ...)
      (database-tables-list->database-tables
       (map parse-database-table tables))))))

(define (read-database db-path)
  (call-with-db-lock
   (parse-database
    (call-with-input-file db-path
      (lambda (db) (read db))))))

(define (save-database db-path db)
  (call-with-db-lock
   (call-with-output-file db-path
     (lambda (db-p)
       (pretty-print
	`(database
	  ,@(map
	     (lambda (dt)
	       `(table
		 (channel-name ,(channel-table-name dt))
		 (channel-url ,(channel-table-channel-url dt))
		 (last-updated ,(date->string (channel-table-last-updated dt)
					      db-date-format))
		 (entries
		  ,(map
		    (lambda (e)
		      `(entry
			(title ,(entry-title e))
			(description ,(entry-description e))
			(link ,(entry-link e))
			(date ,(entry-date e))))
		    (channel-table-entries dt)))))
	     (database-tables->database-tables-list (database-tables db))))
	db-p)))))

;;
;; Table operations
;;

(define (table-cleanup table store-size)
  (let ((table-entries (channel-table-entries table)))
    (make-channel-table (channel-table-name table)
			(channel-table-channel-url table)
			(channel-table-last-updated table)
			(list-head table-entries
				   (min store-size
					(length table-entries))))))

;; TODO: fix assumption that the table is populated
(define (table-insert table entries store-size)
  (let* ((first-entry (car (channel-table-entries table)))
	 (relevant-entries
	  (take-while
	   (lambda (e) (not (equal? e first-entry)))
	   entries))
	 (updated-channel-table
	  (make-channel-table
	   (channel-table-name table)
	   (channel-table-channel-url table)
	   (current-date)
	   (append relevant-entries
		   (channel-table-entries table))))
	 (final-channel (table-cleanup updated-channel-table store-size)))
    `(,final-channel ,relevant-entries)))

;;
;; Database operations
;;

(define (database-get db table-name)
  (hash-table-ref/default (database-tables db) table-name #f))

(define (database-has-table db table-name)
  (hash-table-exists? (database-tables db) table-name))

;; (define (database-update db table-entry)
;;   (hash-table-set! (database-tables db)
;; 		   (channel-table-name table-entry)
;; 		   table-entry))

(define* (database-insert-entries! db config config-entry entries)
  (let* ((table-name (config-entry-name config-entry))
	 (table-url (config-entry-url config-entry))
	 (store-size (configuration-store-size config))
	 (insert-entries-result
	  (if (database-has-table db table-name)
	      (table-insert (database-get db table-name)
			    entries store-size)
	      `(,(make-channel-table
		  table-name table-url (current-date)
		  (list-head entries
			     (min store-size
				  (length entries))))
		,entries)))
	 (new-entries (cadr insert-entries-result))
	 (final-table (car insert-entries-result)))
    (hash-table-set!
     (database-tables db)
     table-name
     final-table)
    new-entries))
