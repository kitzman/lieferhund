;;
;; Copyright © 2021 kitzman <kitzman @ disroot . org>
;;

(define-module (lieferhund hooks script)
  #:export (make-each-script-hook))
;;
;; script-cmd-fmt format example:
;; cat "~~CONTENT~~" | sendmail
;; cat "~~CONTENT~~" > "/tmp/~~NAME~~.txt"
;;
;; content-fmt:
;; '("From: xxx@yyy.zzz\n"
;;   "To: mmm@nnn.ppp\n"
;;   "Subject: ~~DATE~~ -- ~~TITLE~~\n"
;;   "Hello this is me and I want to inform "
;;   "you about ~~TITLE~~, from ~~NAME~~, which can be "
;;   "found at ~~LINK~~. The contents can be found below:\n"
;;   "~~DESCRIPTION~~")
;;

(define (make-each-script-hook script-cmd-fmt content-fmt)
  `(process-each
    (lambda (config-entry news-item)
      (call-with-temporary-output-file
       (lambda (content-file content-port)
	 (let ((content (hook-string-interpolator-l
			  ,content-fmt
			  config-entry news-item))
	       (cmd (hook-string-interpolator
		     ,script-cmd-fmt
		     config-entry
		     news-item
		     content-file)))
	   (put-string content-port content)
	   (flush-all-ports) ; for some reason, `close` does not flush...
	   (system cmd)))))))
