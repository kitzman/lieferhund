;;
;; Copyright © 2021 kitzman <kitzman @ disroot . org>
;;

(define-module (lieferhund hooks printer)
  #:use-module (lieferhund processor)
  #:export (make-printer-hook))

(define (make-printer-hook)
  `(process-each
    (lambda (config-entry news-item)
      (format #t "[~a] ~a (~a)\n"
	      (config-entry-name config-entry)
	      (entry-title news-item)
	      (entry-link news-item)))))
