;;
;; Copyright © 2021 kitzman <kitzman @ disroot . org>
;;

(define-module (lieferhund config)
  #:use-module (srfi srfi-9)
  #:use-module (ice-9 match)
  #:use-module (ice-9 pretty-print)

  #:export (<configuration>
	    make-configuration
	    configuration-store-size
	    configuration-entries
	    configuration-post-hook
	    
	    <configuration-entry>
	    make-configuration-entry
	    config-entry-name
	    config-entry-url
	    config-entry-type
	    config-entry-opts
	    
	    parse-config-entry
	    parse-config-file
	    read-config-file
	    save-config-file))

;;
;; Configuration files
;;

(define-record-type <configuration>
  (make-configuration store-size entries post-hook)
  configuration-file?
  (store-size configuration-store-size)
  (entries configuration-entries)
  (post-hook configuration-post-hook))

(define-record-type <configuration-entry>
  (make-configuration-entry name url type opts)
  configuration-entry?
  (name config-entry-name)
  (url config-entry-url)
  (type config-entry-type)
  (opts config-entry-opts))

(define (parse-config-entry exp)
  (match exp
    (('entry ('name name)
	     ('url url)
	     ('type type)
	     ('opts opts))
     (make-configuration-entry name url type opts))))

(define (parse-config-file exp)
  (apply
   make-configuration
   (match exp
     (('configuration
       ('post-hook post-hook)
       ('store-size store-size)
       entries ...)
      `(,store-size ,(map parse-config-entry entries) ,post-hook)))))

(define (read-config-file config-path)
  (parse-config-file
   (call-with-input-file config-path
     (lambda (config-file) (read config-file)))))

(define (save-config-file config-path config)
  (call-with-output-file config-path
    (lambda (config-file)
      (pretty-print
       `(configuration
	 (post-hook ,(configuration-post-hook config))
	 (store-size ,(configuration-store-size config))
	 ,@(map
	    (lambda (ce)
	      `(entry
		(name ,(config-entry-name ce))
		(url ,(config-entry-url ce))
		(type ,(config-entry-type ce))
		(opts ,(config-entry-opts ce))))
	    (configuration-entries config)))
       config-file))))
